﻿using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Reolmarked.Model;
using System.Collections.ObjectModel;
using System.Windows;

namespace Reolmarked.Commands
{
	public class BookStands : ICommand
	{
		public event EventHandler? CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		public bool CanExecute(object? parameter)
		{
			if (parameter is BookingWindow booking)
			{
				if (booking.tbBookMail.Text != null && booking.LejReol.HasItems)
				{
					return true;
				}
			}
			return false;
		}

		public void Execute(object? parameter)
		{
			if (parameter is BookingWindow booking && booking.DataContext is MainViewModel mvm)
			{
				ConfirmDialog dialog = new ConfirmDialog();
				if (dialog.ShowDialog() == true)
				{
					foreach (Stand stand in booking.LejReol.Items)
					{
						mvm.SelectedStands.Add(stand);
					}
					try 
					{ 
						mvm.RentStandsToSeller();
						string reoler = "";
						foreach (Stand stand in mvm.SelectedStands)
						{
							reoler += stand.StandID.ToString() + " ";
						}
						MessageBox.Show($"Reolerne {reoler}\n" +
										$"er lejet ud til\n" +
										$"{booking.tbBookMail.Text}", "Success!");
						mvm.SelectedStands.Clear();
					} 
					catch (Exception) 
					{
						MessageBox.Show("Ingen kunde med den angivne mail", "Fejl!");
					}
					
				}
			}
		}
	}
}
