﻿using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Reolmarked.Commands
{
	public class NewSeller : ICommand
	{
		public event EventHandler? CanExecuteChanged;

		public bool CanExecute(object? parameter)
		{
			return true;
		}

		public void Execute(object? parameter)
		{
			if (parameter is MainViewModel mvm)
			{
				SellerDialog dialog = new()
				{
					DataContext = mvm
				};
				mvm.SelectedSeller = new();
				if (dialog.ShowDialog() == true)
				{
					mvm.CreateSeller();
					mvm.Sellers.Add(mvm.SelectedSeller);
					MessageBox.Show($"Lejer oprettet!\n {dialog.tbdialogFirstName.Text} {dialog.tbdialogLastName.Text}", "Success!");
				}
			}
		}
	}
}
