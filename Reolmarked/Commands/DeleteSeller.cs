﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Reolmarked.Commands
{
	public class DeleteSeller : ICommand
	{
		public event EventHandler? CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		public bool CanExecute(object? parameter)
		{
			if (parameter is MainViewModel mvm)
			{
				if (mvm.SelectedSeller == null)
					return false;
			}
			return true;
		}

		public void Execute(object? parameter)
		{
			if (parameter is MainViewModel mvm)
			{
				ConfirmDialog confirm = new();
				if (confirm.ShowDialog() == true) 
				{
					MessageBox.Show($"Lejer Slettet\n {mvm.SelectedSeller.FirstName} {mvm.SelectedSeller.LastName}", "Success!");
					mvm.DeleteSeller();
					mvm.Sellers.Remove(mvm.SelectedSeller);
				}
			}
		}
	}
}
