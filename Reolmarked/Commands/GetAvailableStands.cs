﻿using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Reolmarked.Commands
{
	public class GetAvailableStands : ICommand
	{
		public event EventHandler? CanExecuteChanged;

		public bool CanExecute(object? parameter)
		{
			return true;
		}

		public void Execute(object? parameter)
		{
			if (parameter is MainViewModel mvm)
			{
				mvm.EndDate = mvm.StartDate.AddDays(7 * mvm.Weeks);
				mvm.RetrieveByRentalPeriod();
			}
		}
	}
}
