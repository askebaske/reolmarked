﻿using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Reolmarked.Commands
{
	public class CreateSeller : ICommand
	{
		public event EventHandler? CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		public bool CanExecute(object? parameter)
		{
			if (parameter is SellerDialog dialog)
			{
				if (dialog.tbdialogFirstName.Text != null
					&& dialog.tbdialogLastName.Text != null
					&& (dialog.tbdialogEmail.Text != null && dialog.tbdialogEmail.Text.Contains('@'))
					&& dialog.tbdialogPhone.Text.Length == 8
					&& dialog.tbdialogAccountNum.Text.Length == 14)
				{
					return true;
				}
			}
			return false;
		}

		public void Execute(object? parameter)
		{
			if (parameter is SellerDialog dialog)
			{
				dialog.DialogResult = true;
			}
		}
	}
}
