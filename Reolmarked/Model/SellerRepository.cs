﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using Microsoft.Data.SqlClient;

namespace Reolmarked.Model
{
	public class SellerRepository
	{
		string connectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;

		private ObservableCollection<Seller> sellers;

        public SellerRepository()
        {
			sellers = new();
			RetrieveAll();
        }

        public ObservableCollection<Seller> RetrieveAll() 
		{
			ObservableCollection<Seller> sellers = new();

			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand("SELECT * FROM Seller", con);
				using (SqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						Seller seller = new Seller();
						seller.SellerID = int.Parse(reader["SellerID"].ToString());
						seller.FirstName = reader["firstName"].ToString();
						seller.LastName = reader["lastName"].ToString();
						seller.Email = reader["email"].ToString();
						seller.Phone = int.Parse(reader["phone"].ToString());
						seller.AccountNum = long.Parse(reader["accountNum"].ToString());

						sellers.Add(seller);
					}
					return sellers;
				}
			}
		}

		public int GetIDFromMail(string email)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"SELECT SellerID FROM Seller WHERE Email = '{email}'", con);
				return Convert.ToInt32(cmd.ExecuteScalar());
			}
		}

		public void DeleteSeller(int sellerID)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"spDeleteSeller {sellerID}", con);
				cmd.ExecuteNonQuery();
			}
		}

		public int CreateSeller(string firstName, string lastName, string email, string accountNum, string phone)
		{
			using (SqlConnection con = new SqlConnection(connectionString)) 
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"INSERT INTO Seller VALUES ('{firstName}', '{lastName}', '{email}', '{accountNum}', '{phone}')" + "SELECT @@IDENTITY", con);
				int sellerID = Convert.ToInt32(cmd.ExecuteScalar());
				SqlCommand cmd2 = new SqlCommand($"INSERT INTO MonthlySale VALUES (0, 0, {sellerID})", con);
				cmd2.ExecuteNonQuery();
				return sellerID;
			}
		}
	}
}
