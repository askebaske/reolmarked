﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reolmarked.Model
{
	public class StandRepository
	{
		string connectionString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;

		public ObservableCollection<Stand> RetrieveAll()
		{
			ObservableCollection<Stand> stands = new ObservableCollection<Stand>();

			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"SELECT * FROM Stand", con);
				using (SqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						Stand stand = new Stand();
						stand.StandID = int.Parse(reader["standID"].ToString());
						stand.Type = reader["type"].ToString();
						stands.Add(stand);
					}
				}
				return stands;
			}
		}

		public ObservableCollection<Stand> RetrieveByRentalPeriod(string startDate, string endDate)
		{
			ObservableCollection<Stand> stands = new ObservableCollection<Stand>();

			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"EXEC spRetrieveByRentalPeriod '{startDate}', '{endDate}'", con);
				using (SqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						Stand stand = new Stand();
						stand.StandID = int.Parse(reader["standID"].ToString());
						stand.Type = reader["type"].ToString();
						stands.Add(stand);
					}
				}
				return stands;
			}
		}

		public ObservableCollection<Stand> GetRentals(string email)
		{
			ObservableCollection<Stand> rentals = new ObservableCollection<Stand>();

			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"EXEC spGetRentalsByEmail '{email}'", con);
				using (SqlDataReader reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						Stand stand = new Stand();
						stand.StandID = int.Parse(reader["standID"].ToString());
						rentals.Add(stand);
					}
				}
				return rentals;
			}
		}

		public int CreateRentalPeriod(string startDate, string endDate, int sellerID)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"INSERT INTO RentalPeriod VALUES ('{startDate}', '{endDate}', {sellerID})" 
					+ $"SELECT rentalID FROM RentalPeriod WHERE sellerID = {sellerID}", con);
				return Convert.ToInt32(cmd.ExecuteScalar());
			}
		}

		public void CreateStandRentalPeriod(int rentalID, int standID)
		{
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				SqlCommand cmd = new SqlCommand($"EXEC spInsertStandRentalPeriod {rentalID}, {standID}", con);
				cmd.ExecuteNonQuery();
			}
		}
	}
}
