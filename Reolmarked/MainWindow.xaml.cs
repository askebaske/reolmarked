﻿using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Reolmarked
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		MainViewModel mvm = new MainViewModel();
		public MainWindow()
		{
			InitializeComponent();
			DataContext = mvm; // datacontext sættes til en instans af MainViewModel
		}

		private void NavigateToSellerWindow(object sender, RoutedEventArgs e)
		{
			// åbner et SellerWindow og gir datacontext med videre
			SellerWindow sellerWindow = new SellerWindow();
			sellerWindow.DataContext = this.DataContext;
			sellerWindow.Show();
		}

		private void NavigateToBookingWindow(object sender, RoutedEventArgs e)
		{
			// åbner et BookingWindow og gir datacontext med videre
			BookingWindow bookingWindow = new BookingWindow();
			bookingWindow.DataContext = this.DataContext;
			bookingWindow.Show();
		}

		private void MediaElement_MediaEnded(object sender, RoutedEventArgs e)
		{
			// Gør at baggrundsvideo kører i loop
			MediaElement media = sender as MediaElement;
			media.Position = TimeSpan.Zero;
			if (media.Position == TimeSpan.Zero)
			{
				media.Play();
			}		
		}
	}
}
