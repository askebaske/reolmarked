﻿using Reolmarked.Model;
using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Reolmarked
{
    /// <summary>
    /// Interaction logic for BookingWindow.xaml
    /// </summary>
    public partial class BookingWindow : Window
    {
        public BookingWindow()
        {
            InitializeComponent();
            LejReol.ItemsSource = LedigeReoler.SelectedItems;
        }

		private void DecrementWeeks(object sender, RoutedEventArgs e)
		{
            if (DataContext is MainViewModel mvm)
            {
                mvm.Weeks--;
                if (mvm.Weeks < 1) 
                {
                    mvm.Weeks = 1;
                }
            }
        }

        private void IncrementWeeks(object sender, RoutedEventArgs e)
        {
            if (DataContext is MainViewModel mvm)
                mvm.Weeks++;
        }
	}
}
