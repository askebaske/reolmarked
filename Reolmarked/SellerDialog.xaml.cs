﻿using Reolmarked.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Reolmarked
{
	/// <summary>
	/// Interaction logic for SellerDialog.xaml
	/// </summary>
	public partial class SellerDialog : Window
	{
		public SellerDialog()
		{
			InitializeComponent();
		}

		private void FalseResultClick(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
