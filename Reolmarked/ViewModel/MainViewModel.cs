﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Microsoft.IdentityModel.Tokens;
using Reolmarked.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Reolmarked.Commands;
using System.Diagnostics;

namespace Reolmarked.ViewModel
{
	public partial class MainViewModel : ObservableObject
	{
		// instantiering af repositories
		private StandRepository standRepo = new();
		private SellerRepository sellerRepo = new();

		// Stands og Sellers fyldes med info fra Databasen
		public MainViewModel()
		{
			Stands = standRepo.RetrieveAll();
			Sellers = sellerRepo.RetrieveAll();
		}

		/* 
		 *     Binds + metoder til Seller objekter herunder
		 *     
		 *     
		*/
		public ObservableCollection<Seller> Sellers { get; set; }

		// Properties til at binde et seller object fra view
		[ObservableProperty]
		private Seller selectedSeller;

		public void DeleteSeller()
		{
			sellerRepo.DeleteSeller(SelectedSeller.SellerID);
		}

		public void CreateSeller()
		{
			// en oprettet lejer sendes til databases og der returneres det id som databasen har givet lejeren
			SelectedSeller.SellerID = sellerRepo.CreateSeller(SelectedSeller.FirstName, SelectedSeller.LastName, SelectedSeller.Email, SelectedSeller.AccountNum.ToString(), SelectedSeller.Phone.ToString());
		}


		/*
		 *		Binds + Metoder til stand objekter herunder
		 *		
		 *		
		*/

		// Property til BookingWindow email textbox
		[ObservableProperty]
		private string searchmail;

		[ObservableProperty]
		private string bookingmail;

		// lister der er bindet til view i BookingWindow
		public ObservableCollection<Stand> Stands { get; set; } = new();
		public ObservableCollection<Stand> RentedStands { get; set; } = new();
		public ObservableCollection<Stand> SelectedStands { get; set; } = new();

		// Properties til at binde leje datoer til/fra view (BookingWindow)
		[ObservableProperty]
		private DateTime startDate = DateTime.Now;
		[ObservableProperty]
		private double weeks = 1;
		[ObservableProperty]
		private DateTime endDate = DateTime.Now.AddDays(7); // sætter slutdato til x antal uger efter startdato

		public void GetRentals()
		{
			RentedStands.Clear();
			ObservableCollection<Stand> list = new();
			list = standRepo.GetRentals(Searchmail);
			foreach (Stand stand in list)
			{
				RentedStands.Add(stand);
			}
		}

		public void RetrieveByRentalPeriod()
		{
			Stands.Clear();
			string start = StartDate.ToString("yyyy-MM-dd");
			string end = EndDate.ToString("yyyy-MM-dd");
			ObservableCollection<Stand> stands = standRepo.RetrieveByRentalPeriod(start, end);
			foreach (Stand stand in stands)
			{
				this.Stands.Add(stand);
			}
		}

		public void RentStandsToSeller()
		{
			// Lejeperiode oprettes i database og der returneres et id fra sql query
			int id = standRepo.CreateRentalPeriod(StartDate.ToString("yyyy-MM-dd"), 
				EndDate.ToString("yyyy-MM-dd"), 
				sellerRepo.GetIDFromMail(Bookingmail));

			// det returnerede id bruges til at tilknytte reoler til den rigtige lejeperiode
			foreach (Stand stand in SelectedStands)
			{
				standRepo.CreateStandRentalPeriod(id, stand.StandID);
			}
		}


		/*
		 *		Commands til knapper i View herunder
		 *		se implementering i Commands mappen
		 * 
		*/ 
		public ICommand NewSellerCmd { get; set; } = new NewSeller();
		public ICommand CreateSellerCmd { get; set; } = new CreateSeller();
		public ICommand DeleteSellerCmd { get; set; } = new DeleteSeller();
		public ICommand BookStandsCmd { get; set; } = new BookStands();
		public ICommand GetAvailableStandsCmd { get; set; } = new GetAvailableStands();
		public ICommand GetSellerStandsCmd { get; set; } = new GetSellerStands();
	}
}
